package ir.ac.shahreza.computer.spec.homework3.model.pojo;

/**
 * Created by Pouyan on 28/Dec/2017.
 */
public class Rank {
    private int id;
    private String degree;
    private String caption;
    private long salary;

    //<editor-fold desc="Getters and Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }
    //</editor-fold>
}
