package ir.ac.shahreza.computer.spec.homework3.model.pojo;

/**
 * Created by Pouyan on 27/Dec/2017.
 */
public class Proj {
    private int id;
    private String loc;
    private String caption;
    private String end;
    private String begin;

    //<editor-fold desc="Getters and Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }
    //</editor-fold>
}
