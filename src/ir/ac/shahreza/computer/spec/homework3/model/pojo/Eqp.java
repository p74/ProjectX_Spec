package ir.ac.shahreza.computer.spec.homework3.model.pojo;

/**
 * Created by Pouyan on 28/Dec/2017.
 */
public class Eqp {
    private int id;
    private String proj_fk;
    private String loc;
    private String stat;
    private int emp_fk;

    //<editor-fold desc="Getters and Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProj_fk() {
        return proj_fk;
    }

    public void setProj_fk(String proj_fk) {
        this.proj_fk = proj_fk;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public int getEmp_fk() {
        return emp_fk;
    }

    public void setEmp_fk(int emp_fk) {
        this.emp_fk = emp_fk;
    }
    //</editor-fold>
}