package ir.ac.shahreza.computer.spec.homework3.model.pojo;

/**
 * Created by Pouyan on 27/Dec/2017.
 */

public class Emp {
    private int id;
    private String name;
    private String phone;
    private String degree;
    private int rank_fk;
    private int proj_fk;

    //<editor-fold desc="Getters and Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public int getRank_fk() {
        return rank_fk;
    }

    public void setRank_fk(int rank_fk) {
        this.rank_fk = rank_fk;
    }

    public int getProj_fk() {
        return proj_fk;
    }

    public void setProj_fk(int proj_fk) {
        this.proj_fk = proj_fk;
    }
    //</editor-fold>
}
