package ir.ac.shahreza.computer.spec.homework3.ctrl.interfaces;

import javax.swing.text.Document;
import java.awt.*;
import java.util.TreeMap;

public interface xReader {
    /**
     * Reads an XML file from the given
     * path and returns it as a {@code Document}
     * @param path
     * @return
     */
    Document readXML(String path);

    /**
     * Uses an internal path
     * @return
     */
    Document readXML();

    /**
     * Can be used in tree views
     * @return {@code TreeMap}
     */
    TreeMap treeView();

    /**
     * Can be used in list views
     * @return {@code List}
     */
    List listView();
}
