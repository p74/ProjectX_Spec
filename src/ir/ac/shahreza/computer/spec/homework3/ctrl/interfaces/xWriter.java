package ir.ac.shahreza.computer.spec.homework3.ctrl.interfaces;

import ir.ac.shahreza.computer.spec.homework3.model.pojo.Emp;
import ir.ac.shahreza.computer.spec.homework3.model.pojo.Eqp;
import ir.ac.shahreza.computer.spec.homework3.model.pojo.Proj;
import ir.ac.shahreza.computer.spec.homework3.model.pojo.Rank;

/**
 * The implementation of this interface must
 * include all nessecery functions for
 * {@code INSERT}, {@code UPDATE} and {@code DELETE}
 */
public interface xWriter {
    /**
     * The implementations of this function-set
     * are suppose to act as {@code INSERT}, creating
     * new records in the XML database.
     * @param emp gets a POJO and creates a new record
     *            based upon it.
     */
    //<editor-fold desc="Insert functions">
    void newEmp(Emp emp);
    void newEqp(Eqp eqp);
    void newProj (Proj proj);
    void newRank (Rank rank);
    //</editor-fold>

    /**
     * The implementations of this function-set
     * are suppose to act as {@code UPDATE}, updating
     * existing records in the XML database.
     * @param emp gets a POJO and updates an existing
     *            record based upon it.
     */
    //<editor-fold desc="Update functions">
    void editEmp(Emp emp);
    void editEqp(Eqp eqp);
    void editProj (Proj proj);
    void editRank (Rank rank);
    //</editor-fold>

    /**
     * The implementations of this function-set
     * are supposed to act as {@code DELETE}, removing
     * existing records in the XML database.
     * @param emp gets a POJO and deletes an existing
     *            record based upon it.
     */
    //<editor-fold desc="Delete functions">
    void deleteEmp(Emp emp);
    void deleteEqp(Eqp eqp);
    void deleteProj (Proj proj);
    void deleteRank (Rank rank);
    //</editor-fold>
}
